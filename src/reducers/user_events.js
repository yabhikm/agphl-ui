const initialState = [];

const user_events = (state = initialState, { type, payload}) => {
    switch(type) {
        case 'GET_USER_EVENTS_SUCCESS':
            state = payload;
        break;

        case 'GET_USER_EVENTS_FAIL':
        case 'GET_USER_EVENTS_LOADING':
            state = initialState;
        break;

        default:
        break;
    }

    return state;
};

export default user_events;