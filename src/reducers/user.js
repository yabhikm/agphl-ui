const initialState = {
    id: null,
    auth: null,
};

const user = (state = initialState, { type, payload = {}}) => {
    switch(type) {
        case 'USER_AUTH_FAIL':
            state = {
                ...state,
                auth: false
            };
        break;
        case 'USER_AUTH_LOADING':
            state = initialState;
        break;

        case 'USER_AUTH_SUCCESS':
            state = {
                ...payload,
                auth: true
            };
        break;

        default:
        break;
    }
    return state;
};

export default user;