const initialState = {
    loading: false
};

const app = (state = initialState, { type, payload = {}}) => {
    switch(type) {
        case 'USER_AUTH_LOADING':
        case 'GET_EVENTS_LOADING':
            state = {
                loading: true
            };
        break;

        case 'USER_AUTH_FAIL':
        case 'USER_AUTH_SUCCESS':
        case 'GET_EVENTS_SUCCESS':
        case 'GET_EVENTS_FAIL':
            state = {
                loading: false
            };
        break;

        default:
        break;
    }
    return state;
};

export default app;