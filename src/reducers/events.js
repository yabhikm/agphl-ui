const initialState = {
    types: []
};

const events = (state = initialState, { type, payload }) => {
    switch(type) {
        case 'GET_EVENTS_LOADING':
        case 'GET_EVENTS_FAIL':
            state = initialState;
        break;

        case 'GET_EVENTS_SUCCESS':
            state = {
                ...state,
                types: payload
            };
        break;

        default:
        break;
    }
    return state;
};

export default events;