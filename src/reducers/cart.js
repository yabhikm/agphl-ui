const initialState = {
    user: {},
    type: {},
    time: null,
    schedule: {}
};

const cart = (state = initialState, { type, payload}) => {
    switch(type) {
        case 'GET_USER_EVENTS_LOADING':
            state = {
                ...state
            };
        break;

        case 'SELECT_TYPE':
            state = {
                ...state,
                type: {...payload}
            };
        break;

        case 'SELECT_TIME':
            state = {
                ...state,
                time: payload
            };
        break;

        case 'GET_USER_LOADING':
        case 'GET_USER_FAIL':
            state = {
                ...state,
                user: {}
            };
        break;

        case 'GET_USER_SUCCESS':
            state = {
                ...state,
                user: payload
            };
        break;

        case 'ADD_SCHEDULE_SUCCESS':
            state = {
                ...state,
                schedule: payload
            };
        break;

        default:
        break;
    }

    return state;
};

export default cart;