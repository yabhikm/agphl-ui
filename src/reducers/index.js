import app from './app';
import user from './user';
import events from './events';
import cart from './cart';
import user_events from './user_events';

export default {
    app,
    user,
    events,
    cart,
    user_events
};