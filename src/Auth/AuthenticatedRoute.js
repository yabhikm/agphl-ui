import React from 'react';
import { Route } from 'react-router-dom';

import RedirectToExternal from '../components/RedirectToExternal';

// const url = 'http://localhost/agpl/api/public/login';
const url = 'http://167.172.122.82/logout';

const AuthenticatedRoute = ({ component: C, appProps, ...rest }) => {
    return (
        <Route
            {...rest}
            render={props =>
                appProps.isAuthenticated
                ? <C {...props} {...appProps} />
                : <RedirectToExternal url={url} />
            }
        />
    );
}

export default AuthenticatedRoute;