import React from 'react';
import { Route } from 'react-router-dom';
import { Redirect } from 'react-router-dom';

const UnauthenticatedRoute = ({ component: C, appProps, ...rest }) => {
    return (
        <Route
            {...rest}
            render={props =>
                !appProps.isAuthenticated
                ? <C {...props} {...appProps} />
                : <Redirect url="/my-schedule" />
            }
        />
    );
}

export default UnauthenticatedRoute;