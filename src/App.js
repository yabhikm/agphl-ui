import React, { useState, useEffect } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Cookies from 'universal-cookie';

import store from './store';

import './App.css';
import NotFound from './components/NotFound';
import AuthenticatedRoute from './Auth/AuthenticatedRoute';
import UnauthenticatedRoute from './Auth/UnauthenticatedRoute';
import ScheduleMeeting from './containers/ScheduleMeeting';
import MySchedule from './containers/MySchedule';
import EventApp from './EventApp';
import Token from './containers/Token';

function App() {
  const [isAuthenticated, userHasAuthenticated] = useState(undefined);
  useEffect(() => {
    const cookies = new Cookies();
    userHasAuthenticated(!!cookies.get('token'));
  }, []);

  return (
    <Provider store={store}>
      <Router>
        <EventApp>
          {
            isAuthenticated === undefined ? (<div>Authenticating...</div>) : (
              <Switch>
                <UnauthenticatedRoute
                  path="/schedule-meeting/:id"
                  component={ScheduleMeeting}
                  appProps={{ isAuthenticated }}
                />
                <AuthenticatedRoute
                  path="/my-schedule"
                  component={MySchedule}
                  appProps={{ isAuthenticated }}
                />
                <Route path="/token/:token" component={Token} />
                <Route component={NotFound} />
              </Switch>
            )
          }
        </EventApp>
      </Router>
    </Provider>
  );
}

export default App;
