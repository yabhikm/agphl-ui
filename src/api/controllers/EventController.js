import requestService from '../services/requestService';

const getEvents = () => {
  const request = {
    method: 'get',
    url: `/event-types`,
    params: {
      type: 'get_event_types',
    }
  };

  return requestService(request);
};

const addEvent = (data, query) => {
  const request = {
    method: 'post',
    url: `/event-types`,
    params: {
      type: 'add_event_types',
    },
    data
  };

  return requestService(request);
};

const updateEvent = (data, query) => {
  const request = {
    method: 'patch',
    url: `/event-types/${query.id}`,
    params: {
      type: 'update_event_types',
    },
    data
  };

  return requestService(request);
};

const getUserEvents = (query) => {
  const request = {
    method: 'get',
    url: `/user/${query.id}/event-types`,
    params: {
      type: 'get_user_event_types',
    }
  };

  return requestService(request);
};

export default {
  getEvents,
  addEvent,
  updateEvent,
  getUserEvents
};
