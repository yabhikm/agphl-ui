import requestService from '../services/requestService';

const authenticate = () => {
  const request = {
    method: 'get',
    url: `/auth`,
    params: {
      type: 'user_auth',
    }
  };

  return requestService(request);
};

export default {
  authenticate
};
