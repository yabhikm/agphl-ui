import requestService from '../services/requestService';

const getUserAvailability = ({ user, type, date}) => {
  const request = {
    method: 'get',
    url: `/user/${user}/availability`,
    params: {
      type,
      date
    }
  };

  return requestService(request);
};

const getUser = ({ id }) => {
  const request = {
    method: 'get',
    url: `/user/${id}`,
    params: {
      type: 'get_user'
    }
  };

  return requestService(request);
};

const addSchedule = (data, query) => {
  const request = {
    method: 'post',
    url: `/schedule`,
    params: {
      type: 'add_schedule',
    },
    data
  };

  return requestService(request);
};

const getUpcoming = () => {
  const request = {
    method: 'get',
    url: `/schedule/upcoming`,
    params: {
      type: 'get_schedule_upcoming'
    }
  };

  return requestService(request);
};

const getPast = () => {
  const request = {
    method: 'get',
    url: `/schedule/past`,
    params: {
      type: 'get_schedule_past'
    }
  };

  return requestService(request);
};

export default {
  getUserAvailability,
  getUser,
  addSchedule,
  getUpcoming,
  getPast
};
