import AuthController from './controllers/AuthController';
import EventController from './controllers/EventController';
import UserController from './controllers/UserController';

const routes = {
  get: {},
  post: {},
  patch: {}
};

// GET
routes.get['/auth'] = AuthController.authenticate;
routes.get['/event-types'] = EventController.getEvents;
routes.get['/user/:id/event-types'] = EventController.getUserEvents;
routes.get['/user/:id/availability'] = UserController.getUserAvailability;
routes.get['/user/:id'] = UserController.getUser;
routes.get['/schedule/upcoming'] = UserController.getUpcoming;
routes.get['/schedule/past'] = UserController.getPast;

// POST
routes.post['/event-types'] = EventController.addEvent;
routes.post['/schedule'] = UserController.addSchedule;

// PATCH
routes.patch['/event-types/:id'] = EventController.updateEvent;


export default routes;
