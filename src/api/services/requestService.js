import { httpResponse } from '../helpers';
import server from '../server';
import errorService from './errorService';
import Cookies from 'universal-cookie';

export default (request) =>
  new Promise((resolve, reject) => {
    const cookies = new Cookies();
    const token = cookies.get('token') || '';
    server({
      ...request,
      'Content-Type':  'application/json',
      headers: {
        Authorization: token
      }
    })
      .then(response => {
        if ([200, 201].includes(response.status)) {
          const data = response.data;
          resolve(httpResponse(200, data));
        } else {
          const error = `${response.message || `Something went wrong `}`;
          reject(
            httpResponse(400, {
              message: error,
            }),
          );
        }
      })
      .catch(errorService(reject));
  });
