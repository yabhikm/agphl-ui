import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Paper,
    Grid,
    Card,
    CardContent,
    CardActions,
    List,
    ListItem,
    ListItemText,
    ListItemAvatar,
    ListItemSecondaryAction,
    Tabs,
    Tab,
    Divider,
    Typography
} from '@material-ui/core';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import api from '../api';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    grid: {
        padding: theme.spacing(6)
    },
    actionWrap: {
        flexDirection: 'column',
        paddingBottom: '20px'
    },
    value: {
        textAlign: 'left',
        padding: '5px'
    },
    label: {
        textAlign: 'left',
        padding: '5px'
    }
}));

const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

const ScheduledEvents = () => {
    const [ activeMenu, setActiveMenu ] = useState(0);
    const classes = useStyles();
    const [schedule, setSchedule] = useState([]);
    useEffect(() => {
        api.get('/schedule/upcoming').then(res => {
            setSchedule(res.data);
        }).catch(() => {
            setSchedule([]);
        });
    }, []);

    const handleTabChange = (event, newValue) => {
        setSchedule([]);
        switch(newValue) {
            case 0:
                api.get('/schedule/upcoming').then(res => {
                    setSchedule(res.data);
                }).catch(() => {
                    setSchedule([]);
                });
            break;

            case 1:
                api.get('/schedule/past').then(res => {
                    setSchedule(res.data);
                }).catch(() => {
                    setSchedule([]);
                });
            break;
        }
        setActiveMenu(newValue);
    };

    return <Grid item xs={12}>
        <Paper className={classes.paper}>
            <Grid container spacing={1}>
                <Grid item xs={2}></Grid>
                <Grid item xs={8} className={classes.grid}>
                    <Card className={classes.root} variant="outlined">
                        <CardContent>
                            <Tabs
                                value={activeMenu}
                                indicatorColor="primary"
                                textColor="primary"
                                onChange={handleTabChange}
                            >
                                <Tab label="Upcoming" />
                                <Tab label="Past" />
                            </Tabs>
                            <Divider />
                            <List component="nav" aria-label="main mailbox folders">
                                {
                                    schedule.map(i => {
                                        const dt = new Date(i.from);
                                        return (
                                            <React.Fragment key={i.id}>
                                                <ListItem
                                                    selected={false}
                                                >
                                                    <ListItemAvatar>
                                                        <FiberManualRecordIcon color='primary'></FiberManualRecordIcon>
                                                    </ListItemAvatar>
                                                    <ListItemText primary={`${dt.getHours()}:${('0'+dt.getMinutes()).slice(-2)}, ${days[dt.getDay()]} ${months[dt.getMonth()]} ${dt.getDate()}, ${dt.getFullYear()} `} />
                                                    <ListItemSecondaryAction>
                                                        <Typography component="h6" variant="h6">
                                                            {`${i.f_name} ${i.l_name}`}
                                                        </Typography>
                                                        Event Type: <b>{i.title}</b>
                                                    </ListItemSecondaryAction>
                                                </ListItem>
                                                <Divider />
                                            </React.Fragment>
                                        )}
                                    )
                                }
                            </List>
                            <Divider />
                        </CardContent>
                        <CardActions className={classes.actionWrap}>
                            Pagination
                        </CardActions>
                    </Card>
                </Grid>
            </Grid>
        </Paper>
    </Grid>
}

export default ScheduledEvents;