import React, { useEffect } from 'react';

const RedirectToExternal = ({ url }) => {
    useEffect(() => {
        window.location.href = url;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return <div>Redirecting...</div>
};

export default RedirectToExternal;
