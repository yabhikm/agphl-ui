import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Typography,
    Paper,
    Grid,
    Chip
} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import api from '../api';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        color: theme.palette.text.secondary,
    },
    timeWrap: {
        textAlign: 'center',
        display: 'flex',
        flexDirection: 'column'
    },
    timeItem: {
        marginBottom: '5px'
    }
}));

const isDateDisabled = ({ date }) => [0, 6].includes(date.getDay());
const minDate = new Date();
const maxDate = new Date();
minDate.setDate(minDate.getDate() + 1);
maxDate.setDate(maxDate.getDate() + 14);

const Step1 = ({ cart, handleSelectTime, onBack }) => {
    const classes = useStyles();
	const [ selectedDate, setSelectedDate ] = useState(cart.time);
    const [ availableTime, setAvailableTime ] = useState([]);
    
    const onDateChange = date => {
        if (date) {
            setSelectedDate(date);
            getAvailableTime(date);
        }
    }

    useEffect(() => {
        if(cart.time) {
            getAvailableTime(cart.time);
        }
    }, [])
    
    const getAvailableTime = (dt) => {
        const user = cart.user.id;
        const date =`${dt.getFullYear()}${('0' + (dt.getMonth() + 1)).slice(-2)}${('0' + dt.getDate()).slice(-2)}`;
        const type = cart.type.id;
        api.get('/user/:id/availability', { params: { date, type, user } }).then(res => {
            const { data = [] } = res;
            setAvailableTime(data || []);
        }).catch(() => {
            setAvailableTime([]);
        });
    }

    const onTimeSelect = index => {
        const selectedTime = availableTime[index] || null;
		if (selectedTime) {
			selectedDate.setHours(selectedTime.h_24);
            selectedDate.setMinutes(selectedTime.m);
            handleSelectTime(selectedDate);
        }
    };
    
    return <Grid item xs={12}>
        <Paper className={classes.paper}>
            <Grid container spacing={1}>
                <Grid item xs={3}>
                    <a href="/#" onClick={e => {e.preventDefault(); onBack(2)}}><ArrowBackIcon color="primary" /></a>
                    <Typography variant="h6" component="h1">
                        {cart.type.title}
                    </Typography>
                    <p><QueryBuilderIcon fontSize="small" /> {cart.type.duration} min</p>
                </Grid>
                <Grid item xs={5}>
                    <Calendar
                        onChange={onDateChange}
                        maxDate={maxDate}
                        minDate={minDate}
                        view="month"
                        tileDisabled={isDateDisabled}
                        value={cart.time || null}
                    />
                </Grid>
                <Grid item xs={4} className={classes.timeWrap}>
                    {
                        availableTime.map((i, index) => (
                            <React.Fragment key={index}>
                                <Chip
                                    label={`${i.h_12}:${i.m} ${i.period}`}
                                    clickable
                                    color="primary"
                                    onClick={() => onTimeSelect(index)}
                                    variant="outlined"
                                    className={classes.timeItem}
                                />
                            </React.Fragment>
                        ))
                    }
                </Grid>
            </Grid>
        </Paper>
    </Grid>
};

export default Step1;