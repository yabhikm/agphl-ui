import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import {
    Typography,
    Paper,
    Grid,
    List,
    ListItem,
    ListItemText,
    ListItemAvatar,
    Divider
} from '@material-ui/core';
import { green } from '@material-ui/core/colors';
import DateRangeIcon from '@material-ui/icons/DateRange';
import FiberManualRecordIcon  from '@material-ui/icons/FiberManualRecord';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));
const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

const Step4 = ({ cart }) => {
    const classes = useStyles();
    const from = new Date(cart.schedule.from);
    const to = new Date(cart.schedule.to);
    return <Grid item xs={12}>
        <Paper className={classes.paper}>
            <Grid container spacing={1}>
                <Grid item xs={12}>
                    <Typography variant="h6" component="h1">
                        Confirmed
                    </Typography>
                    <p>
                        You are scheduled with {cart.user.name}
                    </p>
                </Grid>
            </Grid>

            <Grid container spacing={1}>
                <Grid item xs={4}></Grid>
                <Grid item xs={4}>
                    <List component="nav" aria-label="main mailbox folders">
                        <Divider />
                        <ListItem>
                            <ListItemAvatar>
                                <FiberManualRecordIcon color='primary'></FiberManualRecordIcon>
                            </ListItemAvatar>
                            <ListItemText>
                                {cart.type.title}
                                <p style={{ color: green[500] }}>
                                    <DateRangeIcon fontSize="small" />
                                    {`${from.getHours()}:${('0'+from.getMinutes()).slice(-2)} - ${to.getHours()}:${('0'+to.getMinutes()).slice(-2)}, ${days[from.getDay()]} ${months[from.getMonth()]} ${from.getDate()}, ${from.getFullYear()} `}
                                </p>
                            </ListItemText>
                        </ListItem>
                        <Divider />
                    </List>
                    <p><ArrowForwardIcon color="primary" /><Link to={`/schedule-meeting/${cart.user.id}`}>Schedule another event</Link></p>
                </Grid>
            </Grid>
        </Paper>
    </Grid>
};

export default Step4;