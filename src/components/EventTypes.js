import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Typography,
    Paper,
    Grid,
    Card,
    CardContent,
    Button,
    CardActions,
    FormControl,
    InputLabel,
    Input,
    Divider,
    Chip
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    grid: {
        padding: theme.spacing(6)
    },
    actionWrap: {
        flexDirection: 'row',
        paddingBottom: '20px',
        textAlign:'center'
    },
    value: {
        textAlign: 'left',
        padding: '5px'
    },
    label: {
        textAlign: 'left',
        padding: '5px'
    },
    root: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
        '& > *': {
            margin: theme.spacing(0.5),
        },
    }
}));

const getStepData = ({ pageType, event_types, actionWrap, root, handleInput, values, handleClick, handleSave, onEdit }) => {
    const duration = [15, 30, 45, 60];
    switch(pageType) {
        case 'list':
        return (
            <>
                {event_types.map(i =>
                    <Grid xs={4} item key={i.id}>
                        <Card variant="outlined">
                            <CardContent>
                                <Typography variant="h5" component="h2">
                                    {i.title}
                                </Typography>
                                <small>{i.duration} mins</small>
                            </CardContent>
                            <CardActions className={actionWrap}>
                                <Button variant="contained" color="primary" onClick={() => onEdit(i.id)}>Edit</Button>
                            </CardActions>
                        </Card>
                    </Grid>
                )}
            </>
        );

        case 'add':
        case 'edit':
        return (
            <>
                <Grid xs={2} item></Grid>
                <Grid xs={8} item>
                    <Card variant="outlined">
                        <CardContent>
                            <Grid container spacing={1}>
                                <Grid xs={12} item>
                                    <FormControl>
                                        <InputLabel htmlFor="amount">Event Name</InputLabel>
                                        <Input type="text" id="event_name" onChange={e => handleInput(e.target.value, 'name')} value={values.name} />
                                    </FormControl>
                                </Grid>

                                <Grid xs={12} item className={root}>
                                </Grid>
                            </Grid>
                            <Grid container spacing={1} className={root}>
                                {duration.map(i => <Grid xs={1} item>
                                    <Chip onClick={() => handleInput(i, 'min')} label={`${i} min`} color={i === values.min ? 'primary' : ''} />
                                </Grid>)}
                                <Grid item xs={3}><Input type="text" placeholder="Custom min" value={duration.includes(values.min) ? '' : values.min} onChange={e => handleInput(+e.target.value, 'min')} /></Grid>
                            </Grid>
                        </CardContent>
                        <Divider />
                        <CardActions className={actionWrap}>
                            <Button variant="contained" onClick={() => handleClick('list')}>Cancel</Button>
                            <Button variant="contained" color="primary" onClick={handleSave}>Save</Button>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid xs={2} item></Grid>
            </>
        );
        default:
        break;
    }
}

const initialValues = {
    id: 0,
    min: 0,
    name: ''
};
const EventTypes = ({ event_types, onAddType, onUpdateType, user }) => {
    const classes = useStyles();
    const [pageType, setPageType] = useState('list');
    const [values, setValues] = useState(initialValues);

    useEffect(() => {
        setPageType('list');
        setValues(initialValues);
    }, [event_types.length]);
    const handleClick = type => {
        setPageType(type);
        setValues(initialValues);
    };

    const handleInput = (value, field) => {
        const updatedValues = values;
        updatedValues[field] = value;
        setValues({...updatedValues});
    };

    const handleSave = () => {
        if (values.min && values.name) {
            pageType === 'add' ? onAddType(values) : onUpdateType(values);
        }
    };

    const onEdit = id => {
        setPageType('edit');
        const selected = event_types.filter(i => i.id === id)[0] || {};
        setValues({
            id: +selected.id,
            min: +selected.duration,
            name: selected.title
        });
    };
    
    return (
        <Grid item xs={12}>
            <Paper className={classes.paper}>
                <Grid container spacing={1}>
                    <Grid item xs={1}>
                        {
                            pageType === 'list' ? (
                                <>
                                    <Typography variant="h6" component="h1">
                                        My Link:
                                    </Typography>
                                    {user.id && <a href="/#" onClick={e => e.preventDefault()}>{`${window.location.host}/#/schedule-meeting/${user.id}`}</a>}
                                </>
                            ) : (
                                <Button variant="outlined" size="small" color="primary" className={classes.margin} onClick={() => handleClick('list')}>
                                    {`< Back`}
                                </Button>
                            )
                        }
                    </Grid>
                    <Grid item xs={9}>
                        {
                            ['add', 'edit'].includes(pageType) && <Typography variant="h6" component="h1">{pageType === 'add' ? 'Add' : 'Edit'} Event Type</Typography>
                        }
                    </Grid>
                    <Grid item xs={2}>
                        {pageType === 'list' && <Button variant="outlined" size="small" color="primary" onClick={() => handleClick('add')} className={classes.margin}>
                            + New Event Type
                        </Button>}
                    </Grid>
                    <Grid item xs={12} className={classes.grid}>
                        <Grid container spacing={1}>
                            {
                                getStepData({
                                    pageType,
                                    event_types,
                                    actionWrap: classes.actionWrap,
                                    root: classes.root,
                                    handleClick,
                                    handleInput,
                                    values,
                                    handleSave,
                                    onEdit
                                })
                            }
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    );
};

export default EventTypes;