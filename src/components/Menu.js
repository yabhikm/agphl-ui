import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Typography,
    Paper,
    Grid,
    Tabs,
    Tab
} from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));

const MySchedule = ({ activeMenu, handleTabChange }) => {
    const classes = useStyles();
    return <Grid item xs={12}>
        <Paper className={classes.paper}>
            <Grid container>
                <Grid item xs={2}>
                    <Typography>My Schedule <ArrowDropDownIcon /></Typography>
                </Grid>
                <Grid item xs={10}></Grid>
                <Grid item xs={12}>
                    <Tabs
                        value={activeMenu < 0 ? 0 : activeMenu}
                        indicatorColor="primary"
                        textColor="primary"
                        onChange={handleTabChange}
                    >
                        <Tab label="Event Types" />
                        <Tab label="Scheduled Events" />
                    </Tabs>
                </Grid>
            </Grid>
        </Paper>
    </Grid>
};

export default MySchedule;