import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Typography,
    Paper,
    Grid,
    List,
    ListItem,
    ListItemText,
    ListItemAvatar,
    ListItemSecondaryAction,
    Divider
} from '@material-ui/core';
import ArrowForwardIosIcon from'@material-ui/icons/ArrowForwardIos';
import FiberManualRecordIcon  from '@material-ui/icons/FiberManualRecord';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));

const Step1 = ({ onTypeSelect, user_events, cart }) => {
    const classes = useStyles();
    return <Grid item xs={12}>
        <Paper className={classes.paper}>
            <Grid container spacing={1}>
                <Grid item xs={12}>
                    <Typography variant="h6" component="h1">
                        {cart.user.name}
                    </Typography>
                    <p>
                        Welcome to my scheduling page. Please follow the instructions to add an event to my calendar
                    </p>
                </Grid>
            </Grid>

            <Grid container spacing={1}>
                <Grid item xs={4}></Grid>
                <Grid item xs={4}>
                    <List component="nav" aria-label="main mailbox folders">
                        <Divider />
                        {
                            user_events.map(i => (
                                <React.Fragment key={i.id}>
                                    <ListItem
                                        button
                                        onClick={() => onTypeSelect(i.id)}
                                        key={i.id}
                                    >
                                        <ListItemAvatar>
                                            <FiberManualRecordIcon color='primary'></FiberManualRecordIcon>
                                        </ListItemAvatar>
                                        <ListItemText primary={i.title} />
                                        <ListItemSecondaryAction>
                                            <ArrowForwardIosIcon />
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                    <Divider />
                                </React.Fragment>
                            ))
                        }
                    </List>
                </Grid>
            </Grid>
        </Paper>
    </Grid>
};

export default Step1;