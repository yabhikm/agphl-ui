import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Typography,
    Paper,
    Grid,
    Button
} from '@material-ui/core';
import { useLocation } from 'react-router-dom';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Cookies from 'universal-cookie';
import { userAuth } from '../actions';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));

const authRoutes = ['/my-schedule'];

const Header = ({ user, dispatch }) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const location = useLocation();

    useEffect(() => {
        authRoutes.includes(location.pathname) && dispatch(userAuth());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    useEffect(() => {
        if (user.auth === false && authRoutes.includes(location.pathname)) {
            handleLogout();
        }
    }, [user.auth]);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleLogout = () => {
        const cookies = new Cookies();
        cookies.remove('token')
        // window.location.href = `http://localhost/agpl/api/public/logout`;
        window.location.href = `http://167.172.122.82/logout`;
    };

    return (
        <Grid item xs={12}>
            <Paper className={classes.paper}>
                <Grid container>
                    <Grid item xs={1}>
                        <Typography component="h1" variant="h4">
                            Logo
                        </Typography>
                    </Grid>
                    <Grid item xs={9}>
                        
                    </Grid>
                    <Grid item xs={2} className="account-wrap">
                        <Typography component="h1" variant="h4">
                            {user.auth && <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                                {user.name}
                                <ArrowDropDownIcon />
                            </Button>}
                            <Menu
                                id="simple-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={Boolean(anchorEl)}
                                onClose={handleClose}
                            >
                                <MenuItem onClick={handleLogout}>Logout</MenuItem>
                            </Menu>
                        </Typography>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    );
};

export default Header;