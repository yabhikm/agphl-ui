import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Typography,
    Paper,
    Grid,
    Button,
    FormControl,
    InputLabel,
    Input,
    TextField
} from '@material-ui/core';
import { green } from '@material-ui/core/colors';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import DateRangeIcon from '@material-ui/icons/DateRange';
import 'react-calendar/dist/Calendar.css';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        color: theme.palette.text.secondary,
    },
    timeWrap: {
        textAlign: 'center',
        display: 'flex',
        flexDirection: 'column'
    },
    timeItem: {
        marginBottom: '5px'
    }
}));
const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
const initialValues = {
    f_name: {
        value:'',
        valid: true
    },
    l_name: {
        value:'',
        valid: true
    },
    email: {
        value:'',
        valid: true
    },
};
const Step3 = ({ cart, onBack, createSchedule }) => {
    const classes = useStyles();
    const [fields, setFields] = useState(initialValues);
    const from = new Date(cart.time);
    const to = new Date(cart.time);
    const tomin = (+to.getMinutes()) + cart.type.duration;
    to.setMinutes(tomin);

    const fieldChange = (value, field) => {
        const newFields = {...fields};
        newFields[field].value = value;
        const updatedFields = validateFields(newFields);
        setFields({...updatedFields});
    };

    const validateFields = (allFields) => {
        for(const field in allFields) {
            if (allFields.hasOwnProperty(field)) {
                allFields[field].valid = !!allFields[field].value;
                if (field === 'email') {
                    allFields[field].valid = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(allFields[field].value);
                }
            }
        }
        return allFields
    }

    const onSubmit = () => {
        const updatedFields = validateFields(fields);
        setFields({...updatedFields});
        let allValid = false;
        for(const field in updatedFields) {
            if (fields.hasOwnProperty(field)) {
                allValid = !allValid && updatedFields[field].valid
            }
        }
        createSchedule({
            f_name: updatedFields.f_name.value,
            l_name: updatedFields.l_name.value,
            email: updatedFields.email.value
        });
    };

    return <Grid item xs={12}>
        <Paper className={classes.paper}>
            <Grid container spacing={1}>
                <Grid item xs={4}>
                    <a href="/#" onClick={e => {e.preventDefault();  onBack(3)}}><ArrowBackIcon color="primary" /></a>
                    <Typography variant="h6" component="h1">
                        {cart.type.title}
                    </Typography>
                    <p><QueryBuilderIcon fontSize="small" /> {cart.type.duration} min</p>
                    <p style={{ color: green[500] }}>
                        <DateRangeIcon fontSize="small" />
                        {`${from.getHours()}:${('0'+from.getMinutes()).slice(-2)} - ${to.getHours()}:${('0'+to.getMinutes()).slice(-2)}, ${days[from.getDay()]} ${months[from.getMonth()]} ${from.getDate()}, ${from.getFullYear()} `}
                    </p>
                </Grid>
                <Grid item xs={8}>
                    <Grid container spacing={1}>
                        <Grid item xs={6}>
                            <FormControl>
                                <TextField
                                    error={!fields.f_name.valid}
                                    label="First Name"
                                    onChange={e => fieldChange(e.target.value, 'f_name')}
                                    helperText={fields.f_name.valid ? '' : 'Invalid input'}
                                />
                            </FormControl>
                        </Grid>
                        <Grid item xs={6}>
                            <FormControl>
                                <TextField
                                    error={!fields.l_name.valid}
                                    label="Last Name"
                                    onChange={e => fieldChange(e.target.value, 'l_name')}
                                    helperText={fields.l_name.valid ? '' : 'Invalid input'}
                                />
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl>
                                <TextField
                                    error={!fields.email.valid}
                                    label="Email"
                                    onChange={e => fieldChange(e.target.value, 'email')}
                                    helperText={fields.l_name.valid ? '' : 'Invalid input'}
                                />
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <Button variant="contained" color="primary" onClick={onSubmit}>Save</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Paper>
    </Grid>
};

export default Step3;