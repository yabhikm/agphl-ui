import React, { useEffect, useState } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import Cookies from 'universal-cookie';


const Token = ({ match }) => {
    const [redirect, setRedirect] = useState(false);
    useEffect(() => {
        const cookies = new Cookies();
        cookies.set('token', match.params.token, { path: '/' });
        setRedirect(true);
    }, []);
    return redirect ? <Redirect url="/my-schedule" /> : <div>Loading...</div>
};

export default withRouter(Token);