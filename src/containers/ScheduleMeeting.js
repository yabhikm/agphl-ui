import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { getUserEvents, selectType, selectTime, getUser, addSchedule } from '../actions';

import Step1 from '../components/Step1';
import Step2 from '../components/Step2';
import Step3 from '../components/Step3';
import Step4 from '../components/Step4';

const renderStepData = ({ step, user_events, cart, onTypeSelect, handleSelectTime, onBack, handleCreateSchedule }) => {
    switch(step) {
        case 1:
        return <Step1 onTypeSelect={onTypeSelect} user_events={user_events} cart={cart} />

        case 2:
        return <Step2 cart={cart} handleSelectTime={handleSelectTime} onBack={onBack} />

        case 3:
        return <Step3 cart={cart} onBack={onBack} createSchedule={handleCreateSchedule} />

        case 4:
        return <Step4 cart={cart} />

        default:
        return;
    }
};

const ScheduleMeeting = ({ match, user_events, cart, dispatch }) => {
    const { params } = match;
    const [step, setStep] = useState(1);
    const onTypeSelect = id => {
        dispatch(selectType(user_events.filter(i => i.id === id)[0] || {}));
        setStep(2);
    };
    useEffect(() => {
        dispatch(getUser({ params: { id: params.id }}));
    }, []);
    useEffect(() => {
        if (cart.user.id) {
            dispatch(getUserEvents({ params: { id: cart.user.id }}));
        }
    }, [cart.user.id]);
    useEffect(() => {
        if (cart.schedule.id) {
            setStep(4);
        }
    }, [cart.schedule.id]);
    const handleSelectTime = date => {
        dispatch(selectTime(date));
        setStep(3);
    }
    const onBack = from => {
        switch(from) {
            case 2:
                setStep(1);
            break;

            case 3:
                setStep(2);
            break;

            default:
                setStep(1);
            break;
        }
    };

    const handleCreateSchedule = ({ f_name, l_name, email}) => {
        const dt = new Date(cart.time);
        const y = dt.getFullYear();
        const m = ('0' + (dt.getMonth() + 1)).slice(-2);
        const d = ('0' + dt.getDate()).slice(-2);
        const h = ('0' + dt.getHours()).slice(-2);
        const i = ('0' + dt.getMinutes()).slice(-2);
        const s = ('0' + dt.getSeconds()).slice(-2);
        const data = {
            type: cart.type.id,
            date: `${y}${m}${d}${h}${i}${s}`,
            f_name,
            l_name,
            email
        };
        dispatch(addSchedule(data));
    };

    return <>
        {cart.user.name ? renderStepData({
            step,
            user_events,
            onTypeSelect,
            cart,
            handleSelectTime,
            onBack,
            handleCreateSchedule
        }) : <div>Loading...</div>}
    </>
};

const mapStateToProps = state => ({
    user_events: state.user_events || [],
    cart: state.cart || {}
});

export default withRouter(connect(mapStateToProps)(ScheduleMeeting));