import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

import { getEvents, addEvent, updateEvent } from '../actions';

import Menu from "../components/Menu";
import EventTypes from '../components/EventTypes';
import ScheduledEvents from '../components/ScheduledEvents';

const MySchedule = ({ event_types, dispatch, user }) => {
    const [ activeMenu, setActiveMenu ] = useState(0);
    useEffect(() => {
        if (user.auth) {
            dispatch(getEvents());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [user.auth]);

    const handleTabChange = (event, newValue) => {
        setActiveMenu(newValue);
    };

    const onAddType = ({ name, min }) => {
        dispatch(addEvent({ title: name, duration: min}));
    };

    const onUpdateType = ({ name, min, id }) => {
        dispatch(updateEvent({ title: name, duration: min }, { params: { id } }));
    };

    return (
        <>
            <Menu activeMenu={activeMenu} handleTabChange={handleTabChange}></Menu>
            {activeMenu === 0 ? <EventTypes event_types={event_types} user={user} onAddType={onAddType} onUpdateType={onUpdateType} /> : <ScheduledEvents />}
        </>
    );
};

const mapStateToProps = state => ({
    user: state.user || {},
    event_types: state.events.types || []
});
export default connect(mapStateToProps)(MySchedule);