import api from '../api';

const userAuthLoading = () => ({
    type: 'USER_AUTH_LOADING'
});

const userAuthSuccess = payload => ({
    type: 'USER_AUTH_SUCCESS',
    payload
});

const userAuthFail = () => ({
    type: 'USER_AUTH_FAIL'
});

export const userAuth = () => {
    return dispatch => {
        dispatch(userAuthLoading());
        api.get('/auth').then(res => {
            dispatch(userAuthSuccess({ ...res.data }));
        }).catch((e) => {
            dispatch(userAuthFail());
        });
    };
};

const getUserLoading = () => ({
    type: 'GET_USER_LOADING'
});

const getUserSuccess = payload => ({
    type: 'GET_USER_SUCCESS',
    payload
});

const getUserFail = () => ({
    type: 'GET_USER_FAIL'
});

export const getUser = query => {
    return dispatch => {
        dispatch(getUserLoading());
        api.get('/user/:id', query).then(res => {
            dispatch(getUserSuccess(res.data));
        }).catch(() => {
            dispatch(getUserFail());
        });
    };
};

const addScheduleLoading = () => ({
    type: 'ADD_SCHEDULE_LOADING'
});

const addScheduleSuccess = payload => ({
    type: 'ADD_SCHEDULE_SUCCESS',
    payload
});

const addScheduleFail = () => ({
    type: 'ADD_SCHEDULE_FAIL'
});

export const addSchedule = data => {
    return dispatch => {
        dispatch(addScheduleLoading());
        api.post('/schedule', data).then(res => {
            dispatch(addScheduleSuccess(res.data));
        }).catch(() => {
            dispatch(addScheduleFail());
        });
    };
};

const getUpcomingScheduleLoading = () => ({
    type: 'GET_UPCOMING_SCHEDULE_LOADING'
});

const getUpcomingScheduleSuccess = payload => ({
    type: 'GET_UPCOMING_SCHEDULE_SUCCESS'
});

const getUpcomingScheduleFail = () => ({
    type: 'GET_UPCOMING_SCHEDULE_FAIL'
});

export const getUpcomingSchedule = data => {
    return dispatch => {
        dispatch(getUpcomingScheduleLoading());
        api.post('/schedule/upcoming', data).then(res => {
            dispatch(getUpcomingScheduleSuccess(res.data));
        }).catch(() => {
            dispatch(getUpcomingScheduleFail());
        });
    };
};

const getPastScheduleLoading = () => ({
    type: 'GET_PAST_SCHEDULE_LOADING'
});

const getPastScheduleSuccess = payload => ({
    type: 'GET_PAST_SCHEDULE_SUCCESS'
});

const getPastScheduleFail = () => ({
    type: 'GET_PAST_SCHEDULE_FAIL'
});

export const getPastSchedule = data => {
    return dispatch => {
        dispatch(getPastScheduleLoading());
        api.post('/schedule/past', data).then(res => {
            dispatch(getPastScheduleSuccess(res.data));
        }).catch(() => {
            dispatch(getPastScheduleFail());
        });
    };
};