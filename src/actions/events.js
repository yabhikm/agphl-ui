import api from '../api';

const getEventsLoading = () => ({
    type: 'GET_EVENTS_LOADING'
});

const getEventsSuccess = payload => ({
    type: 'GET_EVENTS_SUCCESS',
    payload
});

const getEventsFail = () => ({
    type: 'GET_EVENTS_FAIL'
});

export const getEvents = () => {
    return dispatch => {
        dispatch(getEventsLoading());
        api.get('/event-types').then(res => {
            dispatch(getEventsSuccess(res.data));
        }).catch(() => {
            dispatch(getEventsFail());
        });
    };
};

const addEventLoading = () => ({
    type: 'ADD_EVENT_LOADING'
});

const addEventSuccess = () => ({
    type: 'ADD_EVENT_SUCCESS'
});

const addEventFail = () => ({
    type: 'ADD_EVENT_Fail'
});

export const addEvent = (data) => {
    return dispatch => {
        dispatch(addEventLoading());
        api.post('/event-types', data).then(res => {
            dispatch(addEventSuccess());
            dispatch(getEvents());
        }).catch(() => {
            dispatch(addEventFail());
        });
    };
}

const updateEventLoading = () => ({
    type: 'UPDATE_EVENT_LOADING'
});

const updateEventSuccess = () => ({
    type: 'UPDATE_EVENT_SUCCESS'
});

const updateEventFail = () => ({
    type: 'UPDATE_EVENT_Fail'
});

export const updateEvent = (data, query) => {
    return dispatch => {
        dispatch(updateEventLoading());
        api.patch('/event-types/:id', data, query).then(res => {
            dispatch(updateEventSuccess());
            dispatch(getEvents());
        }).catch(() => {
            dispatch(updateEventFail());
        });
    };
}

const getUserEventsLoading = () => ({
    type: 'GET_USER_EVENTS_LOADING'
});

const getUserEventsSuccess = payload => ({
    type: 'GET_USER_EVENTS_SUCCESS',
    payload
});

const getUserEventsFail = () => ({
    type: 'GET_USER_EVENTS_FAIL'
});

export const getUserEvents = (query) => {
    return dispatch => {
        dispatch(getUserEventsLoading());
        api.get('/user/:id/event-types', query).then(res => {
            dispatch(getUserEventsSuccess(res.data));
        }).catch(() => {
            dispatch(getUserEventsFail());
        });
    };
};