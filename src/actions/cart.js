import api from '../api';

export const selectType = payload => ({
    type: 'SELECT_TYPE',
    payload
});

export const selectTime = payload => ({
    type: 'SELECT_TIME',
    payload
});